﻿using System.Security.Cryptography;
using System.Text;

namespace Framework.Tools.Extentions
{
    public static class StringExtention
    {
        public static string ConvertToHash(this string str)
        {
            StringBuilder builder = new StringBuilder();
            using (SHA256 sha256Hash = SHA256.Create())
            {
                // ComputeHash - returns byte array  
                byte[] bytes = sha256Hash.ComputeHash(Encoding.UTF8.GetBytes(str));

                // Convert byte array to a string   

                for (int i = 0; i < bytes.Length; i++)
                {
                    builder.Append(bytes[i].ToString("x2"));
                }
                builder.ToString();
            }
            return builder.ToString();
        }

    }
}
