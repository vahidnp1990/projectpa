﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Framework.Domain.Queris
{
    public class Pagination<T>
    {
         public Pagination(int page,Limit limit)
        {
            Page = page;
            Limit = limit;
        }
        public Pagination(Query query):this(query.Page,new Limit(query.Limit))
        {
            
        
        }
        public int Page { get;private set; }
        public int Limit { get; private set; }
        public int Total { get; set; }
        public IEnumerable<T> Body { get; set; }
    }
   public class Limit
    {
        public Limit(int limit)
        {
            Value = limit;
        }
        public static Limit FromInt(int limit)=>new Limit(limit);
        public int Value { get;private set; }
       public static implicit operator int(Limit limit)=>limit.Value;
    }
}
