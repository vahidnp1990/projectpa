﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Framework.Domain.Queris
{
   public class Query
    {
        public int Limit { get; set; }
        public int Page { get; set; }
        public int StartPoint () {
            
                return Limit * Page;
             }
    }
}
