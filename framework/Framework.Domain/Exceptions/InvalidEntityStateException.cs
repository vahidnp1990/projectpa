﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Framework.Domain.Exceptions
{
    public class InvalidEntityStateException : Exception
    {
        private string _entityName;
        private string _patternMessage;
        private string _propName="";
        private string _message;

        public InvalidEntityStateException(object entity, string message)
        {
            _patternMessage = "امکان تغییر وضعیت {0} وجود ندارد. {1}";
            _entityName = entity.GetType().Name;
            _message = message ?? "";
        }
        public InvalidEntityStateException(object entity, string message,string propName)
     : this(entity,message)
        {
            this._propName = propName??"";
        }
        public override string Message { get { return LoalizerMessage(); } }
        public string LoalizerMessage()
        {
            //using(var service =ShareDependency.GetScope())
            //{
            //    IStringLocalizer<DataAnnotationTranslate> localizer1 = (IStringLocalizer<DataAnnotationTranslate>)
            //                 service.ServiceProvider.GetService(typeof(IStringLocalizer<DataAnnotationTranslate>));

            //    IStringLocalizer<InvalidEntityStateException> localizer =
            //      (IStringLocalizer<InvalidEntityStateException>)service.ServiceProvider.
            //      GetService(typeof(IStringLocalizer<InvalidEntityStateException>));
            //    string propName = localizer1[_propName];
            //    string entityName = localizer1[_entityName];
            //    string message = localizer1[_message, propName];
            //    return localizer[_patternMessage, entityName, message];
            //}
            return "";
        }
    }
}
