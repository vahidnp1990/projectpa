﻿using Framework.Domain.ApplicationServices;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;

namespace Framework.Domain.Events
{
    public class EventDispatcher : IEventDispatcher
    {
        public EventDispatcher()
        {

        }
        public void Dispatche<T>(params T[] events) where T : IEvent
        {
            var _handlers = AppDomain.CurrentDomain.GetAssemblies().SelectMany(c =>
              c.GetTypes().Where(x => x.GetInterfaces().Any(y => y.IsGenericType && y.GetGenericTypeDefinition() == typeof(ICommandHandler<,>)))).ToList();
            foreach (var @event in events)
            {
                if (@event == null)
                    throw new ArgumentNullException(nameof(@event), "event can not null.");
                foreach (var handler in _handlers)
                {
                    bool canHandlerEvent = handler.GetInterfaces()
                        .Any(x => x.IsGenericType && x.GetGenericTypeDefinition() ==
                        typeof(ICommandHandler<,>) && x.GenericTypeArguments[0] == @event.GetType());
                    if (canHandlerEvent)
                    {
                        var instance = Activator.CreateInstance(handler);
                        var method = instance.GetType().GetRuntimeMethods()
                            .First(c => c.Name.Equals("Handle"));
                        object[] parametersArray = new object[] { @event };
                        method.Invoke(instance, parametersArray);
                    }
                }
            }
        }
    }
}
