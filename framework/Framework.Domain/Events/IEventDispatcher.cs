﻿using Framework.Domain.Events;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Framework.Domain.Events
{
   public interface IEventDispatcher
    {
        void Dispatche<T>(params T[] @event) where T : IEvent;
    }
}
