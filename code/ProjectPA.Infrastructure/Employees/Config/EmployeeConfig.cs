﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using ProjectPA.Domain.Employees.Entites;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ProjectPA.Infrastructure.Employees.Config
{
    public class EmployeeConfig : IEntityTypeConfiguration<Employee>
    {
        public void Configure(EntityTypeBuilder<Employee> builder)
        {
            builder.Property(c => c.LName).HasMaxLength(200);
            builder.Property(c => c.FName).HasMaxLength(200);
            builder.Property(c => c.FatherName).HasMaxLength(200);
            builder.Property(c => c.PictureName).HasMaxLength(200);
            builder.Property(c => c.LName).HasMaxLength(200);
        }
    }
}
