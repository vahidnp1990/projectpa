﻿using Framework.Domain.Queris;
using ProjectPA.ApplicationService.Employees.Dto;
using ProjectPA.ApplicationService.Employees.Queries;
using ProjectPA.Domain.Employees.Data;
using ProjectPA.Domain.Employees.Data.Dto;
using ProjectPA.Domain.Employees.Data.Queries;
using ProjectPA.Domain.Employees.Entites;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ProjectPA.Infrastructure.Employees
{
    public class EmployeeRepository : IEmployeeRepository,IEmployeeQueryServices
    {
        private readonly ContextApp context;

        public EmployeeRepository(ContextApp context)
        {
            this.context = context;
        }

        public void Delete(Employee employee)
        {
            context.Employees.Remove(employee);
        }

        public Employee Load(long id)
        {
          
            return context.Employees.FirstOrDefault(c => c.Id == id);
        }

        public Employee LoadByFName(string fName)
        {
            return context.Employees.FirstOrDefault(c => c.FName == fName);
        }

        public Employee LoadByLName(string lname)
        {
            return context.Employees.FirstOrDefault(c => c.LName == lname);
        }
        public void Save(Employee employee)
        {
            context.Employees.Add(employee);
        }

        public Pagination<EmployeeSummary> Query(EmployeeSearchByName query)
        {
            Pagination<EmployeeSummary> page = new Pagination<EmployeeSummary>(query);
            page.Body = context.Employees.Where(c => c.FName == query.Name).Select(c=>new EmployeeSummary(c.Id,c.FName,c.LName,c.NationCode,c.Mobile));
            return page;
        }

        public Pagination<EmployeeSummary> Query(EmployeeSearchByNationCode query)
        {
            Pagination<EmployeeSummary> page = new Pagination<EmployeeSummary>(query);
            page.Body = context.Employees.Where(c => c.NationCode == query.NationCode).Select(c => new EmployeeSummary(c.Id, c.FName, c.LName, c.NationCode, c.Mobile));
            return page;
        }

        public Pagination<EmployeeSummary> Query(Query query)
        {
            Pagination<EmployeeSummary> page = new Pagination<EmployeeSummary>(query);
            page.Body = context.Employees.Skip(query.StartPoint()).Take(query.Limit).Select(c => new EmployeeSummary(c.Id, c.FName, c.LName, c.NationCode, c.Mobile));
            page.Total = context.Employees.Count();
            return page;
        }

        public EmployeeDto Query(EmployeeFindById query)
        {
            throw new NotImplementedException();
            //return context.Employees.FirstOrDefault(c => c.Id == query.Id);
        }

        public bool Query(FNameValidation query)
        {
            return !context.Employees.Any(c => c.FName == query.FName);
        }

        public bool Query(LNameValidation query)
        {
            return !context.Employees.Any(c => c.LName == query.LName);
        }
    }
 
}
