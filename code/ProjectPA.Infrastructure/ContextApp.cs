﻿using Microsoft.EntityFrameworkCore;
using ProjectPA.Domain.Employees.Entites;
using ProjectPA.Domain.UserProfiles;
using System;
using System.Collections.Generic;
using System.Diagnostics.CodeAnalysis;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ProjectPA.Infrastructure
{
    public class ContextApp : DbContext
    {
        public ContextApp([NotNull] DbContextOptions options) : base(options)
        {

            
        }
        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {

        }
        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.ApplyConfigurationsFromAssembly(GetType().Assembly);

            //long id = 1;
            //modelBuilder.Entity<Employee>().HasData(new {Id=id,FName="",LName="",FatherName="", NationCode="" });
     

        }
        public DbSet<Employee> Employees { get; set; }
        public DbSet<User> Users { get; set; }
        public DbSet<Role> Roles { get; set; }
    }
}
