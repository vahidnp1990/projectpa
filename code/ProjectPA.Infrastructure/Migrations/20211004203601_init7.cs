﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace ProjectPA.Infrastructure.Migrations
{
    public partial class init7 : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DeleteData(
                table: "RoleClime",
                keyColumn: "Id",
                keyValue: 2L);

            migrationBuilder.AlterColumn<string>(
                name: "RoleName",
                table: "Users",
                type: "nvarchar(max)",
                nullable: true,
                oldClrType: typeof(string),
                oldType: "nvarchar(20)",
                oldMaxLength: 20,
                oldNullable: true);

            migrationBuilder.UpdateData(
                table: "RoleClime",
                keyColumn: "Id",
                keyValue: 1L,
                columns: new[] { "Key", "Value" },
                values: new object[] { "Employees", "crud" });

            migrationBuilder.UpdateData(
                table: "RoleClime",
                keyColumn: "Id",
                keyValue: 3L,
                columns: new[] { "Key", "Value" },
                values: new object[] { "Employees", "r" });
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AlterColumn<string>(
                name: "RoleName",
                table: "Users",
                type: "nvarchar(20)",
                maxLength: 20,
                nullable: true,
                oldClrType: typeof(string),
                oldType: "nvarchar(max)",
                oldNullable: true);

            migrationBuilder.UpdateData(
                table: "RoleClime",
                keyColumn: "Id",
                keyValue: 1L,
                columns: new[] { "Key", "Value" },
                values: new object[] { "controller", "Employees" });

            migrationBuilder.UpdateData(
                table: "RoleClime",
                keyColumn: "Id",
                keyValue: 3L,
                columns: new[] { "Key", "Value" },
                values: new object[] { "controller", "EmployeesQuery" });

            migrationBuilder.InsertData(
                table: "RoleClime",
                columns: new[] { "Id", "Key", "RoleId", "Value" },
                values: new object[] { 2L, "controller", 2L, "EmployeesQuery" });
        }
    }
}
