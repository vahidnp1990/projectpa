﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace ProjectPA.Infrastructure.Migrations
{
    public partial class init3 : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.InsertData(
                table: "Employees",
                columns: new[] { "Id", "FName", "FatherName", "LName", "Mobile", "NationCode", "PictureName" },
                values: new object[] { 1L, "", "", "", null, "", null });
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DeleteData(
                table: "Employees",
                keyColumn: "Id",
                keyValue: 1L);
        }
    }
}
