﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace ProjectPA.Infrastructure.Migrations
{
    public partial class init5 : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.InsertData(
                table: "RoleClime",
                columns: new[] { "Id", "Key", "RoleId", "Value" },
                values: new object[] { 1L, "controller", 2L, "Employees" });

            migrationBuilder.InsertData(
                table: "RoleClime",
                columns: new[] { "Id", "Key", "RoleId", "Value" },
                values: new object[] { 2L, "controller", 2L, "EmployeesQuery" });

            migrationBuilder.InsertData(
                table: "RoleClime",
                columns: new[] { "Id", "Key", "RoleId", "Value" },
                values: new object[] { 3L, "controller", 1L, "EmployeesQuery" });
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DeleteData(
                table: "RoleClime",
                keyColumn: "Id",
                keyValue: 1L);

            migrationBuilder.DeleteData(
                table: "RoleClime",
                keyColumn: "Id",
                keyValue: 2L);

            migrationBuilder.DeleteData(
                table: "RoleClime",
                keyColumn: "Id",
                keyValue: 3L);
        }
    }
}
