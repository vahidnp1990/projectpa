﻿using Framework.Domain.Data;
using Microsoft.EntityFrameworkCore.Infrastructure;
using Microsoft.EntityFrameworkCore.Storage;

namespace ProjectPA.Infrastructure
{
    public class UnitOfWork : IUnitOfWork
    {
        private readonly ContextApp context;

        public UnitOfWork(ContextApp context)
        {
            this.context = context;
          
        }
        public int Commit()
        {
            return context.SaveChanges();
        }
    }
}
