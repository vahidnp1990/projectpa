﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using ProjectPA.Domain.UserProfiles;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ProjectPA.Infrastructure.UserProfiles.Config
{
    public class UserConfig : IEntityTypeConfiguration<User>
    {
        public void Configure(EntityTypeBuilder<User> builder)
        {
            builder.Property(c => c.Username).HasMaxLength(50);
            builder.Property(c => c.Password).HasMaxLength(256);
            User user = User.Create("user", "1234").ById(1);
            User admin = User.Create("admin", "1234").ById(2);
            user.RoleSet(Role.Named("user"));
            admin.RoleSet(Role.Named("admin"));
            builder.HasData(user);
            builder.HasData(admin);
        }
    }
}
