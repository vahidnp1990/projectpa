﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using ProjectPA.Domain.UserProfiles;

namespace ProjectPA.Infrastructure.UserProfiles.Config
{
    public class RoleClimeConfig : IEntityTypeConfiguration<RoleClime>
    {
        public void Configure(EntityTypeBuilder<RoleClime> builder)
        {
            builder.Property(c => c.Key).HasMaxLength(50);
            builder.Property(c => c.Value).HasMaxLength(100);

            RoleClime clime = RoleClime.Create("Employees", "crud",2).ById(1);
            RoleClime clime3 = RoleClime.Create("Employees", "r",1).ById(3);
            builder.HasData(clime);
            builder.HasData(clime3);
        }
    }
}
