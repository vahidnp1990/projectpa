﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using ProjectPA.Domain.UserProfiles;

namespace ProjectPA.Infrastructure.UserProfiles.Config
{
    public class RoleConfig : IEntityTypeConfiguration<Role>
    {
        public void Configure(EntityTypeBuilder<Role> builder)
        {
            builder.Property(c => c.Name).HasMaxLength(20);
            builder.HasMany(c => c.Climes).WithOne().OnDelete(DeleteBehavior.Cascade);
            Role roleUser = Role.Named("user").ById(1);
            Role roleAdmin = Role.Named("admin").ById(2);

            builder.HasData(roleUser);
            builder.HasData(roleAdmin);
        }
      
    }
}
