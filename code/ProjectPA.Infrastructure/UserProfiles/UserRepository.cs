﻿using Microsoft.EntityFrameworkCore;
using ProjectPA.Domain.UserProfiles;
using ProjectPA.Domain.UserProfiles.Data;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ProjectPA.Infrastructure.UserProfiles
{
    public class UserRepository : IUserRepository
    {
        private readonly ContextApp context;

        public UserRepository(ContextApp context)
        {
            this.context = context;
        }
        public User Load(string userName)
        {
            User user = context.Users.FirstOrDefault(c => c.Username == userName);
            Role role = context.Roles.Include(c => c.Climes).FirstOrDefault(c => c.Name == user.RoleName);
            user.RoleSet(role);
            return user;
        }
    }
}
