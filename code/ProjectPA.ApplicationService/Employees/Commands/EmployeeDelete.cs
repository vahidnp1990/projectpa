﻿using System.Text.Json.Serialization;

namespace ProjectPA.ApplicationService.Employees.Commands
{
    public class EmployeeDelete
    {
        public long Id { get; set; }
        [JsonIgnore]
        public string OwnerUserName { get; set; }
    }
}
