﻿using Framework.Tools.Validator;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System.ComponentModel.DataAnnotations;
using System.Text.Json.Serialization;

namespace ProjectPA.ApplicationService.Employees.Commands
{
    public class EmployeeCreate
    {
        [Remote("FName", "EmployeeValidation", ErrorMessage ="نام تکراری است")]
        [Display(Name ="نام")]
        [Required(ErrorMessage ="فیلد نام نمی تواند خالی باشد")]
        public string FName { get; set; }
        [Remote("LName", "EmployeeValidation", ErrorMessage ="نام خانوادگی تکراری است")]
        [Display(Name = "نام خانوادگی")]
        [Required(ErrorMessage = "فیلد نام خانوادگی نمی تواند خالی باشد")]
        public string LName { get; set; }
        [Display(Name = "نام پدر")]
        [Required(ErrorMessage = "فیلد نام پدر  نمی تواند خالی باشد")]
        public string FatherName { get; set; }
        [Display(Name = "کد ملی")]
        [Required(ErrorMessage = "فیلد کد ملی نمی تواند خالی باشد")]
        public string NationCode { get; set; }
        [Display(Name = "تلفن همراه")]
        public string Mobile { get; set; }
        [MaxFileSize(1024*1024)]
        [Required(ErrorMessage = "فیلد تصاویر نمی تواند خالی باشد")]
        public IFormFile Picture { get; set; }
        [JsonIgnore]
        public string OwnerUserName { get; set; }
    }
}
