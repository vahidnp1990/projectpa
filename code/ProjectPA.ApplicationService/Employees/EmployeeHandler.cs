﻿using Framework.Domain.ApplicationServices;
using Framework.Domain.Data;
using Framework.Domain.Exceptions;
using ProjectPA.ApplicationService.Employees.Commands;
using ProjectPA.Domain.Employees.Data;
using ProjectPA.Domain.Employees.Entites;

namespace ProjectPA.ApplicationService.Employees
{
    public class EmployeeHandler : 
        ICommandHandler<long,EmployeeChange>,
        ICommandHandler<long,EmployeeCreate>,
        ICommandHandler<bool,EmployeeDelete>
    {
        private readonly IUnitOfWork unitOfWork;
        private readonly IEmployeeRepository empoloyeeRepository;

        public EmployeeHandler(IUnitOfWork unitOfWork,IEmployeeRepository empoloyeeRepository)
        {
            this.unitOfWork = unitOfWork;
            this.empoloyeeRepository = empoloyeeRepository;
        }
        public long Handle(EmployeeChange command)
        {
            Employee employee = empoloyeeRepository.Load(command.Id);
            if(employee==null)
                throw new InvalidEntityStateException(this, "کاربر مورد نظر یافت نشد");
           employee.Update(command.FName, command.LName, command.FatherName, command.NationCode, command.Mobile, command.Picture.FileName);
            employee.Owner(command.OwnerUserName);
            unitOfWork.Commit();
            return employee.Id;
        }

        public long Handle(EmployeeCreate command)
        {
            Employee employee = empoloyeeRepository.LoadByFName(command.FName);
            if(employee!=null)
                throw new InvalidEntityStateException(this, "نام تکراری است");
            employee = empoloyeeRepository.LoadByLName(command.LName);
            if(employee!=null)
                throw new InvalidEntityStateException(this, "نام خانوادگی تکراری است");
            employee = Employee.Create(command.FName, command.LName, command.FatherName, command.NationCode, command.Mobile, command.Picture.FileName);
            employee.Owner(command.OwnerUserName);
            empoloyeeRepository.Save(employee);
            unitOfWork.Commit();
            return employee.Id;
        }

        public bool Handle(EmployeeDelete command)
        {
            Employee employee = empoloyeeRepository.Load(command.Id);
            if (employee == null)
                throw new InvalidEntityStateException(this, "کاربر مورد نظر یافت نشد");
            empoloyeeRepository.Delete(employee);
            employee.Owner(command.OwnerUserName);
            return unitOfWork.Commit()>0;
        }
    }
}
