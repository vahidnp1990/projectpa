﻿using Framework.Domain.ApplicationServices;
using Framework.Domain.Data;
using Framework.Domain.Exceptions;
using ProjectPA.ApplicationService.UserProfiles.Commands;
using ProjectPA.Domain.UserProfiles;
using ProjectPA.Domain.UserProfiles.Data;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ProjectPA.ApplicationService.UserProfiles
{
    public class UserHandler : ICommandHandler<AuthorizationToken,UserLogin>
    {
        private readonly IAuthorizationManager authorization;
        private readonly IUserRepository userRepository;

        public UserHandler(IAuthorizationManager authorization, IUserRepository userRepository)
        {
            this.authorization = authorization;
            this.userRepository = userRepository;
        }
        public AuthorizationToken Handle(UserLogin command)
        {
            User user = userRepository.Load(command.UserName);
            if (user == null)
                throw new InvalidEntityStateException(this, "نام کاربر یا کمله عبور اشتباه است");
            
            return user.Login(authorization,false, command.Password); ;
        }
    }
}
