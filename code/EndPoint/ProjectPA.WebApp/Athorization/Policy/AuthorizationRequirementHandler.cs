﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.Caching.Memory;
using System;
using System.Linq;
using System.Threading.Tasks;

namespace ProjectPA.WebApp.Athorization.Policy
{
    public class AuthorizationRequirementHandler : AuthorizationHandler<AuthoRequirment>
    {
        private IHttpContextAccessor httpContent;
        private readonly IMemoryCache cache;
        private readonly AbortToken abortToken;
        //string actionName="";
        string controllerName;
        string verb = "";
        public AuthorizationRequirementHandler(IHttpContextAccessor httpContent, IMemoryCache cache, AbortToken abortToken)
        {
            this.httpContent = httpContent;
            this.cache = cache;
            this.abortToken = abortToken;
        }
        protected override Task HandleRequirementAsync(AuthorizationHandlerContext context, AuthoRequirment requirement)
        {
            var claims = context.User.Claims;
            bool isAuthoenticated = context.User.Identity.IsAuthenticated;
            verb = httpContent.HttpContext.Request.Method;
            //actionName = httpContent.HttpContext.Request.RouteValues["action"].ToString();
            controllerName = httpContent.HttpContext.Request.RouteValues["controller"].ToString();
            if (isAuthoenticated)
            {
                var jwtExpValue = long.Parse(context.User.Claims.FirstOrDefault(x => x.Type == "exp").Value);
                DateTime expirationTime = DateTimeOffset.FromUnixTimeSeconds(jwtExpValue).DateTime;
                if (expirationTime > abortToken.OnDate.AddDays(1))
                {
                    string value = context.User.Claims.FirstOrDefault(c => c.Type == controllerName).Value;
                    bool isValid = false;
                    if (verb == "POST" && value.Contains('c'))
                        isValid = true;
                    else if (verb == "GET" && value.Contains("r"))
                        isValid = true;
                    else if (verb == "UPDATE" && value.Contains("u"))
                        isValid = true;
                    else if (verb == "DELETE" && value.Contains("d"))
                        isValid = true;

                    if (isValid)
                        context.Succeed(requirement);
                    else
                        context.Fail();
                }

            }
            else
                context.Fail();

            return Task.CompletedTask;
        }
    }
}
