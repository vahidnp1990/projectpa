﻿using Microsoft.Extensions.Configuration;
using Microsoft.IdentityModel.Tokens;
using ProjectPA.Domain.UserProfiles;
using ProjectPA.Domain.UserProfiles.Data;
using System;
using System.Collections.Generic;
using System.IdentityModel.Tokens.Jwt;
using System.Security.Claims;
using System.Text;

namespace Fortnite.EndPoint.Athorization.Identities
{
    public class AuthorizationManager : IAuthorizationManager
    {
        public static string UserId { get { return "UserId"; } }
        public static string UserName { get { return "UsreName"; } }

        private readonly IConfiguration configuration;

        public AuthorizationManager(IConfiguration configuration)
        {

            this.configuration = configuration;
        }

        public AuthorizationToken GeneratToken(long userId,string userName, string role, List<RoleClime> climes, bool rememberMe)
        {
            var authClaims = new List<Claim>() { new Claim(UserId, userId.ToString()), new Claim(UserName, userName) };
            authClaims.Add(new Claim(ClaimTypes.Role, role ?? ""));
            foreach(var clim in climes)
            authClaims.Add(new Claim(clim.Key,clim.Value));
            var authSigningKey = new SymmetricSecurityKey(Encoding.UTF8.GetBytes(configuration["JWT:Secret"]));
            var token = new JwtSecurityToken(
                expires: DateTime.Now.AddDays(rememberMe ? 365 : 1),
                claims: authClaims,
                signingCredentials: new SigningCredentials(authSigningKey, SecurityAlgorithms.HmacSha256)
                );
            return new AuthorizationToken(new JwtSecurityTokenHandler().WriteToken(token), token.ValidTo);
        }
    }


}
