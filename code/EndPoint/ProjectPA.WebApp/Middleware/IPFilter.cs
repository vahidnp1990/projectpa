﻿using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Options;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ProjectPA.WebApp.Middleware
{
    public class IPFilter
    {
        private readonly RequestDelegate _next;
        private readonly IConfiguration configuration;

        public IPFilter(RequestDelegate next, IConfiguration configuration)
        {
            _next = next;
            this.configuration = configuration;
        }

        public async Task Invoke(HttpContext context)
        {
            var clienIps = context.Connection.RemoteIpAddress.ToString();
            string adders= configuration["AdminSafeList"];
            if(!adders.Contains(clienIps))
            {
               await context.Response.WriteAsync("Access denied");
                return;
            }
                
            await _next.Invoke(context);
        }
    }
}
