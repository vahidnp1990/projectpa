﻿using Microsoft.AspNetCore.Mvc;
using ProjectPA.ApplicationService.Employees.Dto;
using ProjectPA.ApplicationService.UserProfiles;
using ProjectPA.ApplicationService.UserProfiles.Commands;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ProjectPA.WebApp.Controllers.Home
{
    public class HomeController : Controller
    {
        public IActionResult Index()
        {
            return View();
        }
        [HttpGet]
        public IActionResult Login()
        {
            return View();
        }
        public IActionResult List()
        {
            return View(new List<EmployeeSummary>());
        }
        public IActionResult CreateEmployee()
        {
            return View();
        }
    }
}
