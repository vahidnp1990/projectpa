using Framework.Domain.Data;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using ProjectPA.Infrastructure;
using ProjectPA.WebApp.Athorization.Policy;
using ProjectPA.WebApp.Attributes;
using ProjectPA.WebApp.Extentions.Services;
using ProjectPA.WebApp.Middleware;

namespace ProjectPA.WebApp
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
           
            services.AddSecurity(Configuration);
            services.AddEmployee();
            services.AddUserPorfile();
            services.AddRazorPages();
            services.AddControllers();
            services.AddDbContext<ContextApp>(c => c.UseSqlServer(Configuration.GetConnectionString("ProjectPA")));
            services.AddScoped(c => new Microsoft.Data.SqlClient.SqlConnection(Configuration.GetConnectionString("ProjectPA")));
            services.AddScoped<IUnitOfWork, UnitOfWork>();
            services.AddTransient<IPFilterAttribute>();
            services.AddSingleton<AbortToken>();
          
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IWebHostEnvironment env)
        {
       
            SeedData.Initialize(app.ApplicationServices);
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }
            else
            {
                app.UseExceptionHandler("/Error");
            }
            app.UseMiddleware<IPFilter>();
            app.UseStaticFiles();

            app.UseRouting();


            app.UseStaticFiles();
            app.UseRouting();
            app.UseHttpsRedirection();
            app.UseAuthentication();
            app.UseAuthorization();
            //app.UseEndpoints(endpoints =>
            //{
            //    endpoints.MapRazorPages();
            //});
            app.UseEndpoints(endpoints =>
            {
                endpoints.MapControllerRoute(
                   name: "default",
                   pattern: "{controller=Home}/{action=Index}/{id?}");
            });
        }
     
      
    }
}
