﻿using Fortnite.EndPoint.Athorization.Identities;
using Microsoft.AspNetCore.Http;
using System.Linq;

namespace ProjectPA.WebApp.Extentions
{
    public static class ExtentionAuthorize
    {
        public static long GetUserId(this HttpContext context)
        {
            long id;
            string strid = context.User.Claims.FirstOrDefault(c => c.Type == AuthorizationManager.UserId).Value;
            long.TryParse(strid, out id);
            return id;
        }
        public static string GetUserName(this HttpContext context)
        {
            string userName = context.User.Claims.FirstOrDefault(c => c.Type == AuthorizationManager.UserName).Value;
            return userName;
        }
    }
}
