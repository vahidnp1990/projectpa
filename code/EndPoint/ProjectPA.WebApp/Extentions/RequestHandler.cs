﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace ProjectPA.WebApp.Extentions
{
    public static class RequestHandler
    {

        public static IActionResult HandleRequest<T,TResult>(this HttpContext httpContext, T request,  Func<T,TResult> handler)
        {
            try
            {
                return new JsonResult(handler(request));
            }
            catch (Exception e)
            {
                //Log Exception
                return new BadRequestObjectResult(new
                {
                    error =
                    e.Message,
                    stackTrace = e.StackTrace
                });
            }
        }
    }
}
