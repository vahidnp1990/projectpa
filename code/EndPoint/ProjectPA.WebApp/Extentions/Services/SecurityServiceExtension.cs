﻿using Fortnite.EndPoint.Athorization.Identities;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.IdentityModel.Tokens;
using ProjectPA.ApplicationService.Employees;
using ProjectPA.ApplicationService.UserProfiles;
using ProjectPA.Domain.Employees.Data;
using ProjectPA.Domain.UserProfiles.Data;
using ProjectPA.Infrastructure.Employees;
using ProjectPA.Infrastructure.UserProfiles;
using ProjectPA.WebApp.Athorization.Policy;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ProjectPA.WebApp.Extentions.Services
{
    public static class SecurityServiceExtension
    {
        public static IServiceCollection AddUserPorfile(this IServiceCollection services)
        {
            services.AddScoped<IUserRepository, UserRepository>();
            services.AddScoped<UserHandler>();
            return services;
        }
        public static IServiceCollection AddEmployee(this IServiceCollection services)
        {
            services.AddScoped<IEmployeeRepository, EmployeeRepository>();
            services.AddScoped<IEmployeeQueryServices, EmployeeRepository>();
            services.AddScoped<EmployeeHandler>();
            return services;
        }
        public static IServiceCollection AddSecurity(this IServiceCollection services, IConfiguration configuration)
        {


            services.AddAuthentication(JwtBearerDefaults.AuthenticationScheme).AddJwtBearer(options =>
            {
                options.SaveToken = true;
                options.RequireHttpsMetadata = false;
                options.TokenValidationParameters = new TokenValidationParameters()
                {
                    ValidateIssuer = false,
                    ValidateAudience = false,
                    ValidateIssuerSigningKey = true,
                    ValidateLifetime = true,
                    IssuerSigningKey = new SymmetricSecurityKey(Encoding.UTF8.GetBytes(configuration["JWT:Secret"]))
                };
                //options.Events = new JwtBearerEvents
                //{
                //    OnMessageReceived = context =>
                //    {
                //        context.Token = context.Request.Cookies["token"];
                //        return Task.CompletedTask;
                //    }
                //};
            
            });
            services.AddSingleton<IHttpContextAccessor, HttpContextAccessor>();
            services.AddSingleton<IAuthorizationHandler, AuthorizationRequirementHandler>();
            services.AddAuthorization(c =>
            {
                c.AddPolicy("policy", p =>
                {
                    p.Requirements.Add(new AuthoRequirment());
                }
                );

            });

            //services.AddScoped<ActionSpcesRepository>();
            //services.AddScoped<VerifyCode>();
            //services.AddScoped<IVerifyCodeMemoryChach, VerifyCodeMemoryChach>();
            services.AddScoped<IAuthorizationManager, AuthorizationManager>();
            return services;
        }
    }
}
