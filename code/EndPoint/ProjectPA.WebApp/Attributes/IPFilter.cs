﻿using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Filters;
using Microsoft.AspNetCore.Routing;
using Microsoft.Extensions.Configuration;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ProjectPA.WebApp.Attributes
{
    public class IPFilterAttribute : ActionFilterAttribute
    {
        private readonly IConfiguration configuration;

        public IPFilterAttribute(IConfiguration configuration)
        {
            this.configuration = configuration;
        }

        public override void OnActionExecuting(ActionExecutingContext context)
        {
            var clienIps = context.HttpContext.Connection.RemoteIpAddress.ToString();
            string adders = configuration["AdminSafeList"];
            if (adders.Contains(clienIps))
            {
                context.Result= new RedirectToRouteResult(
                        new RouteValueDictionary
                        {
                    { "controller", "Home" },
                    { "action", "index" }
                        });

            }
            base.OnActionExecuting(context);
        }

    }
}
