﻿using Framework.Domain.Queris;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using ProjectPA.ApplicationService.Employees;
using ProjectPA.ApplicationService.Employees.Commands;
using ProjectPA.ApplicationService.Employees.Dto;
using ProjectPA.ApplicationService.Employees.Queries;
using ProjectPA.Domain.Employees.Data.Dto;
using ProjectPA.Domain.Employees.Data.Queries;
using ProjectPA.Infrastructure.Employees;
using ProjectPA.WebApp.Extentions;

namespace ProjectPA.WebApp.API.Employees
{
    [Route("api/[controller]")]
    [ApiController]
    // [ServiceFilter(typeof(IPFilterAttribute))]
     [Authorize(AuthenticationSchemes = JwtBearerDefaults.AuthenticationScheme, Policy = "policy")]
    public class EmployeesController : ControllerBase
    {
      
        [HttpPost("Create")]
        public IActionResult Create([FromServices] EmployeeHandler handler, [FromForm]EmployeeCreate command)
        {
            command.OwnerUserName = HttpContext.GetUserName();
            return HttpContext.HandleRequest(command, handler.Handle);
        }
        [HttpPut("Change")]
        public IActionResult Change([FromServices] EmployeeHandler handler, [FromForm] EmployeeChange command)
        {
            command.OwnerUserName = HttpContext.GetUserName();
            return HttpContext.HandleRequest(command, handler.Handle);
        }
        [HttpDelete("Delete")]
        public IActionResult Delete([FromServices] EmployeeHandler handler, [FromBody] EmployeeDelete command)
        {
            command.OwnerUserName = HttpContext.GetUserName();
            return HttpContext.HandleRequest(command, handler.Handle);
        }
    
        [HttpGet("SearchByName")]
        public Pagination<EmployeeSummary> Search([FromServices] IEmployeeQueryServices services, [FromQuery] EmployeeSearchByName query)
        {
            return services.Query(query);
        }
        [HttpGet("SearchByNationCode")]
        public Pagination<EmployeeSummary> Search([FromServices] IEmployeeQueryServices services, [FromQuery] EmployeeSearchByNationCode query)
        {
            return services.Query(query);
        }
        [HttpGet("Find")]
        public EmployeeDto Search([FromServices] IEmployeeQueryServices services, [FromQuery] EmployeeFindById query)
        {
            return services.Query(query);
        }
        [HttpGet("List")]
        public Pagination<EmployeeSummary> Search([FromServices] IEmployeeQueryServices services, [FromQuery] Query query)
        {
            return services.Query(query);
        }
  
    }
}
