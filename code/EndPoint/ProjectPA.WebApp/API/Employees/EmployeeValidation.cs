﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using ProjectPA.Domain.Employees.Data.Queries;
using ProjectPA.Infrastructure.Employees;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ProjectPA.WebApp.API.Employees
{
    [Route("[controller]")]
    [ApiController]
    public class EmployeeValidation : ControllerBase
    {
        [HttpGet("FName")]
        public bool Validation([FromServices] IEmployeeQueryServices services, [FromQuery] FNameValidation query)
        {
            return services.Query(query);
        }
        [HttpGet("LName")]
        public bool Validation([FromServices] IEmployeeQueryServices services, [FromQuery] LNameValidation query)
        {
            return services.Query(query);
        }
    }
}
