﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using ProjectPA.ApplicationService.UserProfiles;
using ProjectPA.ApplicationService.UserProfiles.Commands;
using ProjectPA.WebApp.Athorization.Policy;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ProjectPA.WebApp.API.UsreProfiles
{
    [Route("api/[controller]")]
    [ApiController]
    public class UserController : ControllerBase
    {
        public UserController()
        {

        }
        [HttpPost("Login")]
        public IActionResult Login([FromServices]UserHandler handler,[FromBody] UserLogin command)
        {
          return new JsonResult(handler.Handle(command));
        }
        [HttpPost("AbortToken")]
        public void AbortToken([FromServices] AbortToken abortToken)
        {
            abortToken.OnDate = DateTime.Now;
        }
    }
}
