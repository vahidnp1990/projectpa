﻿using Framework.Domain.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ProjectPA.Domain.Employees.Entites
{
   public class Employee:BaseAggregateRoot<long>
    {
        private Employee()
        {

        }
        public static Employee Create(string fName, string lName, string fatherName, string nationCode, 
            string mobile, string pictureName)
        {
            Employee employee=new Employee();
            employee.FName = fName;
            employee.LName = lName;
            employee.FatherName = fatherName;
            employee.NationCode = nationCode;
            employee.Mobile = mobile;
            employee.PictureName = pictureName;
            return employee;
        }

        public string FName { get; private set; }
        public string LName { get; private set; }
        public string FatherName { get; private set; }
        public string NationCode { get; private set; }
        public string Mobile { get; private set; }
        public string OwnerUserName { get; set; }
        public void Update(string fName, string lName, string fatherName, string nationCode, string mobile, string pictureName)
        {
           FName = fName;
           LName = lName;
           FatherName = fatherName;
           NationCode = nationCode;
           Mobile = mobile;
           PictureName = pictureName;
        }

        public string PictureName { get; private set; }

        public void Owner(string ownerUserName)
        {
            OwnerUserName = ownerUserName;
        }
    }
}
