﻿using Framework.Domain.Queris;
using ProjectPA.ApplicationService.Employees.Dto;
using ProjectPA.ApplicationService.Employees.Queries;
using ProjectPA.Domain.Employees.Data.Dto;
using ProjectPA.Domain.Employees.Data.Queries;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ProjectPA.Infrastructure.Employees
{
   public interface IEmployeeQueryServices
    {
        Pagination<EmployeeSummary> Query(EmployeeSearchByName query);
        Pagination<EmployeeSummary> Query(EmployeeSearchByNationCode query);
        Pagination<EmployeeSummary> Query(Query query);
        EmployeeDto Query(EmployeeFindById query);
        bool Query(FNameValidation query);
        bool Query(LNameValidation query);

    }
}
