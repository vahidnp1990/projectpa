﻿using Framework.Domain.Queris;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ProjectPA.ApplicationService.Employees.Queries
{
    public class EmployeeSearchByName:Query
    {
        public string Name { get; set; }
    }
}
