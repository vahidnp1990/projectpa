﻿namespace ProjectPA.ApplicationService.Employees.Queries
{
    public class EmployeeFindById
    {
        public long Id { get; set; }
    }
}
