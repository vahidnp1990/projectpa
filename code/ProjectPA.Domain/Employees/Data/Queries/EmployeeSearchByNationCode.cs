﻿using Framework.Domain.Queris;

namespace ProjectPA.ApplicationService.Employees.Queries
{
    public class EmployeeSearchByNationCode:Query
    {
        public string NationCode { get; set; }
    }
}
