﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ProjectPA.ApplicationService.Employees.Dto
{
    public class EmployeeSummary
    {
        public EmployeeSummary(long id, string fName, string lName, string nationCode, string mobile)
        {
            Id = id;
            FName = fName;
            LName = lName;
            NationCode = nationCode;
            Mobile = mobile;
        }

        public long Id { get; }
        public string FName { get; }
        public string LName { get; }
        public string NationCode { get; }
        public string Mobile { get; }
    }
}
