﻿using ProjectPA.Domain.Employees.Entites;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ProjectPA.Domain.Employees.Data
{
   public interface IEmployeeRepository
    {
        Employee Load(long id);
        Employee LoadByLName( string lname);
        Employee LoadByFName(string fName);
        void Save(Employee employee);
        void Delete(Employee employee);
    }
}
