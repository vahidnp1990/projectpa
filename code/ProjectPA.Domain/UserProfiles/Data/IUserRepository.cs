﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ProjectPA.Domain.UserProfiles.Data
{
   public interface IUserRepository
    {
        User Load(string userName);

    }
}
