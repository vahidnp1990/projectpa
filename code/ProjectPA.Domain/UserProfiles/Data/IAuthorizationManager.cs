﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ProjectPA.Domain.UserProfiles.Data
{
    public interface IAuthorizationManager
    {
        AuthorizationToken GeneratToken(long userId,string userName,string role, List<RoleClime> climes, bool rememberMe);

    }
    public class AuthorizationToken
    {
        public AuthorizationToken(string token, DateTime expiration)
        {
            Token = token;
            Expiration = expiration;
        }
        public string Token { get; private set; }
        public DateTime Expiration { get; private set; }

    }
}
