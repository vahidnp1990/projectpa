﻿using Framework.Domain.Entities;
using System.Collections.Generic;

namespace ProjectPA.Domain.UserProfiles
{
    public class Role:BaseAggregateRoot<long>
    {
        public string Name { get; private set; }
        public List<RoleClime> Climes { get; private set; } = new List<RoleClime>();
        private Role()
        {

        }
        public static Role Named(string name)
        {

            Role role = new Role();
            role.Name = name;
            return role;
        }
        public Role ById(long id)
        {
            Id = id;
            return this;
        }
        public void ClimeAdd(RoleClime clime)
        {
    
            Climes.Add(clime);
        }
    }
}
