﻿using Framework.Domain.Entities;
using Framework.Domain.Exceptions;
using Framework.Tools.Extentions;
using ProjectPA.Domain.UserProfiles.Data;

namespace ProjectPA.Domain.UserProfiles
{
    public class User:BaseAggregateRoot<long>
    {
        private User()
        {

        }
        public static User Create(string username, string password)
        {
            User user = new User();
            user.Username = username;
            user.PasswordSet(password);
            return user;
        }
        private void PasswordSet(string password)
        {
            Password = password.ConvertToHash();
        }
        public string Username { get; private set; }
        public string Password { get; private set; }
        public string RoleName { get; private set; }
        private Role role=null;
        public void RoleSet(Role role)
        {
            this.role = role;
            RoleName = role.Name;
        }
        public  User ById(long id)
        {
            Id = id;
            return this;
        }

        public AuthorizationToken  Login(IAuthorizationManager authorization,bool rememberMe, string password)
        {
            if(password.ConvertToHash()!=Password)
                throw new InvalidEntityStateException(this, "نام کاربر یا کمله عبور اشتباه است");
            return authorization.GeneratToken(Id, Username, RoleName, role.Climes, rememberMe);
        }
    }
}
