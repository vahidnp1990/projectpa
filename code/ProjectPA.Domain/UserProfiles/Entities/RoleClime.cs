﻿using Framework.Domain.Entities;
using System;

namespace ProjectPA.Domain.UserProfiles
{
    public class RoleClime : BaseEntity<long>
    {

        public static RoleClime Create(string key, string value,long roleId)
        {
            RoleClime roleClime = new RoleClime();
            roleClime.Key = key;
            roleClime.Value = value;
            roleClime.RoleId = roleId;
            return roleClime;
        }
        private RoleClime()
        {

        }
        public string Key { get; private set; }
        public string Value { get; private set; }
        public long RoleId { get;private set; }
        public RoleClime ById(long id)
        {
            Id = id;
            return this;
        }

    }
}
