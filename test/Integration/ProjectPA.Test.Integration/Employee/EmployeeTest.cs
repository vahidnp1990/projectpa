﻿using FizzWare.NBuilder;
using FluentAssertions;
using ProjectPA.ApplicationService.Employees;
using ProjectPA.ApplicationService.Employees.Commands;
using ProjectPA.ApplicationService.UserProfiles;
using ProjectPA.Domain.Employees.Data;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xunit;

namespace ProjectPA.Test.Integration.Employee
{
   public class EmployeeTest:PersistenTest
    {

        [Fact]
        public void Create_Employee()
        {
            EmployeeCreate expect = Builder<EmployeeCreate>.CreateNew().With(c=>c.Picture=CreateTestFormFile("Test.jpg")).Build();
            EmployeeHandler handler = GetScope<EmployeeHandler>();
            var repository = GetScope<IEmployeeRepository>();

            long id=handler.Handle(expect);
            var actual =  repository.Load(id);

            actual.Should().BeEquivalentTo(new { expect.FName, expect.LName, expect.FatherName, expect.Mobile, PictureName = expect.Picture.FileName });
        }
        [Fact]
        public void Change_Employee()
        {
            EmployeeCreate create = Builder<EmployeeCreate>.CreateNew().With(c => c.Picture = CreateTestFormFile("Test.jpg")).Build();
            EmployeeHandler handler = GetScope<EmployeeHandler>();
            var repository = GetScope<IEmployeeRepository>();
            long id = handler.Handle(create);
            EmployeeChange expect = Builder<EmployeeChange>.CreateNew().With(c=>c.FName=Faker.Name.First())
                .With(c=>c.FatherName=Faker.Name.First()).With(c=>c.Id=id)
                .With(c => c.Picture = CreateTestFormFile("Test2.jpg")).Build();
            handler.Handle(expect);
            var actual = repository.Load(id);

            actual.Should().BeEquivalentTo(new { expect.FName, expect.LName, expect.FatherName, expect.Mobile, PictureName = expect.Picture.FileName });
        }
        [Fact]
        public void Delete_Employee()
        {
            EmployeeCreate create = Builder<EmployeeCreate>.CreateNew().With(c => c.Picture = CreateTestFormFile("Test.jpg")).Build();
            EmployeeHandler handler = GetScope<EmployeeHandler>();
            var repository = GetScope<IEmployeeRepository>();

            long id = handler.Handle(create);
            EmployeeDelete delete = new EmployeeDelete() { Id = id };
            handler.Handle(delete);

            var actual = repository.Load(id);
            actual.Should().BeNull();
        }
    }
}
