﻿using Microsoft.AspNetCore;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using ProjectPA.WebApp;
using System;
using System.IO;
using System.Transactions;

namespace ProjectPA.Test.Integration
{
    public abstract class PersistenTest : IDisposable
    {
        private TransactionScope scope;
        private readonly IWebHost webhost;
        private bool isSave = false;

        public PersistenTest()
        {
            scope = new TransactionScope();
            webhost = WebHost.CreateDefaultBuilder().UseStartup<Startup>().Build();
        }
        public T GetScope<T>()
        {
            return (T)webhost.Services.GetService(typeof(T));
        }
        public IFormFile CreateTestFormFile(string p_Name)
        {
            byte[] memory = new byte[] { };
            return new FormFile(
                baseStream: new MemoryStream(memory),
                baseStreamOffset: 0,
                length: memory.Length,
                name: "Data",
                fileName: p_Name
            );
        }
        public void IsPersistSave(bool isSave)
        {
            this.isSave = isSave;
        }
        public void Dispose()
        {
            if (!isSave)
                scope.Dispose();
        }
    }
}
