using FluentAssertions;
using ProjectPA.ApplicationService.UserProfiles;
using ProjectPA.ApplicationService.UserProfiles.Commands;
using ProjectPA.Domain.UserProfiles.Data;
using Xunit;

namespace ProjectPA.Test.Integration
{
    public class User:PersistenTest
    {
        [Fact]
        public void when_user_login_Should_be_jwt()
        {
            UserHandler handler = GetScope<UserHandler>();
            AuthorizationToken key =(AuthorizationToken)handler.Handle(new UserLogin { Password = "1234", UserName = "user" });
            key.Token.Should().NotBeNull();
        }
    }
}
