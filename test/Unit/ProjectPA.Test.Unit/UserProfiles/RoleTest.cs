﻿using FluentAssertions;
using ProjectPA.Domain.UserProfiles;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xunit;

namespace ProjectPA.Test.Unit.UserProfiles
{
  public  class RoleTest
    {
        [Fact]
        public void role_create()
        {
            string name = "admin";
            Role role =Role.Named(name);
            role.Name.Should().Be(name);
        }
       
        [Fact]
        public void create_clime()
        {
            string key = "contorler";
            string value = "home";
            var clime = RoleClime.Create(key, value,1);
            clime.Key.Should().Be(key);
            clime.Value.Should().Be(value);

        }
        [Fact]
        public void role_add_access()
        {
            string name = "admin";
            string key = "contorler";
            string value = "home";
            Role role = Role.Named(name);
            var clime = RoleClime.Create(key, value,role.Id);
            role.ClimeAdd(clime);
            role.Climes.Should().HaveCount(1);
            role.Climes.First().Should().BeEquivalentTo(clime);

        }
    }

}
