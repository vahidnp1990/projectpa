﻿using FluentAssertions;
using Framework.Tools.Extentions;
using Xunit;
using ProjectPA.Domain.UserProfiles;
using ProjectPA.Domain.UserProfiles.Data;
using NSubstitute;
using System.Collections.Generic;
using FizzWare.NBuilder;
using System.Linq;
using System;

namespace ProjectPA.Test.Unit.UserProfiles
{
    public class UserTest
    {

        [Fact]
        public void user_create()
        {
            string username = "user1";
            string password = "1234";
            User user =User.Create(username, password);

            user.Username.Should().Be(username);
            user.Password.Should().Be(password.ConvertToHash());
        }
        [Fact]
        public void user_add_role()
        {
            string username = "user1";
            string password = "1234";
            User user =  User.Create(username, password);
            Role role = Role.Named("admin");
            user.RoleSet(role);
            user.RoleName.Should().Be(role.Name);
        }
        [Fact]
        public void when_user_login_Should_be_jwt()
        {
            //IUserRepository userRepository = Substitute.For<IUserRepository>();
            //userRepository.Load("user").Returns();
            var user = User.Create("user", "1234");
           // user.Login();
        }
        [Fact]
        public void when_generatToken_Should_be_jwt()
        {
            IAuthorizationManager authorization = Substitute.For<IAuthorizationManager>();
            long userId=1;
            string role = "user";
            List<RoleClime> climes = Builder<RoleClime>.CreateListOfSize(4).Build().ToList();
            authorization.GeneratToken(userId,"user1", role, climes, false);
            
        }
    }
}
