﻿using FluentAssertions;
using ProjectPA.ApplicationService.Employees.Commands;
using ProjectPA.Domain.Employees.Entites;
using Xunit;

namespace ProjectPA.Test.Unit.Employees
{
    public class EmployeeTest
    {
        [Fact]
        public void Create_Employee()
        {
            string fName = Faker.Name.First();
            string lName = Faker.Name.Last();
            string fatherName = Faker.Name.First();
            string nationCode = "228334011";
            string mobile = Faker.Phone.Number();
            string pictureName ="file.jpg";
            Employee employee =  Employee.Create(fName,lName,fatherName,nationCode,mobile,pictureName);

            employee.Should().BeEquivalentTo(new { FName = fName, LName = lName, FatherName = fatherName, Mobile = mobile, PictureName = pictureName });
        }
        [Fact]
        public void Update_Employee()
        {
            //EmployeeChange employeeChange = new EmployeeChange();
            //EmployeeHandler
        }

    }
}
